package com.culture.auth;



import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;


/**
 * @Title: AuthApplication
 * @Author sn
 * @Package com.culture.auth
 * @Date 2024/3/15 11:38
 * @description:
 */

@SpringBootApplication
@ComponentScan("com.culture")
@MapperScan("com.auth.**.dao")
public class AuthApplication {
    public static void main(String[] args) {
        SpringApplication.run(AuthApplication.class, args);

    }
}


