package com.culture.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;

/**
 * @Title: GatewayApplication
 * @Author sn
 * @Package com.culture.gateway
 * @Date 2024/3/15 15:13
 * @description:
 */

@SpringBootApplication
@ComponentScan("com.culture")
public class GatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(GatewayApplication.class,args);
    }
}
