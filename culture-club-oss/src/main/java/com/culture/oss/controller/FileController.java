package com.culture.oss.controller;

import com.culture.oss.service.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Title: FileController
 * @Author sn
 * @Package com.culture.oss.controller
 * @Date 2024/3/12 20:24
 * @description:
 */

@RequestMapping("/minio")
@RestController
public class FileController {
    @Autowired
    private FileService fileService;
    @GetMapping
    public String getBucket() throws Exception {
        List<String> allBucket = fileService.getAllBucket();
        return allBucket.get(0);
    }
}
