package com.culture.oss.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Getter
@AllArgsConstructor
public enum ResultCodeEnum {

    SUCCESS(200, "成功"),
    FAIL(500, "失败");

    public int code;

    public String desc;

    private static Map<Integer, ResultCodeEnum> cache;

    static {
        cache = Arrays.stream(ResultCodeEnum.values())
                .collect(Collectors.toMap(ResultCodeEnum::getCode, Function.identity()));
    }

    public static ResultCodeEnum of(Integer type) {
        return cache.get(type);
    }

}
