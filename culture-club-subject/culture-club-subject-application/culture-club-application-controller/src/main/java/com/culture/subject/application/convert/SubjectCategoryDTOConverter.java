package com.culture.subject.application.convert;

import com.culture.subject.application.dto.SubjectCategoryDTO;
import com.culture.subject.domain.entity.SubjectCategoryBO;
import com.culture.subject.infra.basic.entity.SubjectCategory;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import org.mybatis.spring.annotation.MapperScan;

import java.util.List;

/**
 * @Title: SubjectController
 * @Author sn
 * @Package com.culture.subject.application.controller
 * @Date 2024/3/3 21:12
 * @description:刷题
 */


@Mapper
public interface SubjectCategoryDTOConverter {
    SubjectCategoryDTOConverter INSTANCE = Mappers.getMapper(SubjectCategoryDTOConverter.class);

    SubjectCategoryBO convertDtoTOBo(SubjectCategoryDTO subjectCategoryDTO);

    SubjectCategoryDTO convertBoToCategoryDTO(SubjectCategoryBO subjectCategoryBO);



    List<SubjectCategoryDTO> convertBoToDTOList(List<SubjectCategoryBO> categoryBOList);




}
