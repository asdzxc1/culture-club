package com.culture.subject.application.convert;


import com.culture.subject.application.dto.SubjectLabelDTO;
import com.culture.subject.domain.entity.SubjectLabelBO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @Title: SubjectController
 * @Author sn
 * @Package com.culture.subject.application.controller
 * @Date 2024/3/3 21:12
 * @description:标签
 */
@Mapper
public interface SubjectLabelDTOConverter {

    SubjectLabelDTOConverter INSTANCE = Mappers.getMapper(SubjectLabelDTOConverter.class);

    SubjectLabelBO convertDtoToLabelBO(SubjectLabelDTO subjectLabelDTO);

    List<SubjectLabelDTO> convertBOToLabelDTOList(List<SubjectLabelBO> boList);

}
