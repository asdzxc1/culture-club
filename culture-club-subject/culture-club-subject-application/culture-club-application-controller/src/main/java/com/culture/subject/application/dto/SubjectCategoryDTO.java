package com.culture.subject.application.dto;

import lombok.Data;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.Serializable;

/**
 * @Title: SubjectController
 * @Author sn
 * @Package com.culture.subject.application.controller
 * @Date 2024/3/3 21:12
 * @description:刷题
 */


@Data
public class SubjectCategoryDTO implements Serializable {

    /**
     * 主键
     */
    private Long id;
    /**
     * 分类名称
     */
    private String categoryName;
    /**
     * 分类类型
     */
    private Integer categoryType;
    /**
     * 图标连接
     */
    private String imageUrl;
    /**
     * 父级id
     */
    private Long parentId;



}