package com.culture.subject.application.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @Title: SubjectLabelBO
 * @Author sn
 * @Package com.culture.subject.domain.entity
 * @Date 2024/3/8 11:01
 * @description:
 */
@Data
public class SubjectLabelDTO implements Serializable {
    /**
     * 主键
     */
    private Long id;
    /**
     * 标签分类
     */
    private String labelName;
    /**
     * 排序
     */
    private Integer sortNum;

    private Long categoryId;

}
