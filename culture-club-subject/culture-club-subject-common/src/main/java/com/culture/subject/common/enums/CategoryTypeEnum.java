package com.culture.subject.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * 分类类型枚举
 * 

 */
@Getter
@AllArgsConstructor
public enum CategoryTypeEnum {

    PRIMARY(1,"岗位大类"),
    SECOND(2,"二级分类");

    public int code;

    public String desc;

    private static Map<Integer, CategoryTypeEnum> cache;

    static {
        cache = Arrays.stream(CategoryTypeEnum.values())
                .collect(Collectors.toMap(CategoryTypeEnum::getCode, Function.identity()));
    }

    public static CategoryTypeEnum of(Integer type) {
        return cache.get(type);
    }

}
