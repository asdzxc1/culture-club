package com.culture.subject.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * 删除状态枚举
 *
 * @author: sn
 */
@AllArgsConstructor
@Getter
public enum IsDeletedFlagEnum {

    DELETED(1, "已删除"),
    UN_DELETED(0, "未删除");

    private int code;

    private String desc;

    private static Map<Integer, IsDeletedFlagEnum> cache;

    static {
        cache = Arrays.stream(IsDeletedFlagEnum.values())
                .collect(Collectors.toMap(IsDeletedFlagEnum::getCode, Function.identity()));
    }

    public static IsDeletedFlagEnum of(Integer type) {
        return cache.get(type);
    }
}
