package com.culture.subject.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * 题目类型枚举
 * 1单选 2多选 3判断 4简答
 * @author: ChickenWing
 * @date: 2023/10/3
 */
@Getter
@AllArgsConstructor
public enum SubjectInfoTypeEnum {

    RADIO(1,"单选"),
    MULTIPLE(2,"多选"),
    JUDGE(3,"判断"),
    BRIEF(4,"简答"),
    ;

    public int code;

    public String desc;


    private static Map<Integer, SubjectInfoTypeEnum> cache;
    static {
        cache = Arrays.stream(SubjectInfoTypeEnum.values())
                .collect(Collectors.toMap(SubjectInfoTypeEnum::getCode, Function.identity()));
    }

    public static SubjectInfoTypeEnum of(Integer type) {
        return cache.get(type);
    }

}
