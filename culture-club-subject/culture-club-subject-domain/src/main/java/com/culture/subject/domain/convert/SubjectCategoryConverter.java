package com.culture.subject.domain.convert;


import com.culture.subject.domain.entity.SubjectCategoryBO;
import com.culture.subject.infra.basic.entity.SubjectCategory;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @Title: SubjectCategoryConverter
 * @Author sn
 * @Package com.culture.subject.domain.convert
 * @Date 2024/3/6 14:22
 * @description:
 */


@Mapper
public interface SubjectCategoryConverter {

    SubjectCategoryConverter INSTANCE = Mappers.getMapper(SubjectCategoryConverter.class);

    SubjectCategory convertBoToCategory(SubjectCategoryBO SubjectCategoryBO);

    List<SubjectCategoryBO> convertBoToCategory(List<SubjectCategory> categoryList);
}
