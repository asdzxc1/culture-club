package com.culture.subject.domain.handler.subject;


import com.culture.subject.common.enums.SubjectInfoTypeEnum;
import com.culture.subject.domain.entity.SubjectInfoBO;
import com.culture.subject.domain.entity.SubjectOptionBO;

public interface SubjectTypeHandler {

    /**
     * 枚举身份的识别
     */
    SubjectInfoTypeEnum getHandlerType();

    /**
     * 实际的题目的插入
     */
    void add(SubjectInfoBO subjectInfoBO);


    SubjectOptionBO query(int intValue);
}
