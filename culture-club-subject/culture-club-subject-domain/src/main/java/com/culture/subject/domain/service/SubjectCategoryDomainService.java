package com.culture.subject.domain.service;

import com.culture.subject.domain.entity.SubjectCategoryBO;
import com.culture.subject.infra.basic.entity.SubjectCategory;

import java.util.List;

/**
 * @Title: SubjectCategoryService
 * @Author sn
 * @Package com.culture.subject.domain.service
 * @Date 2024/3/6 14:13
 * @description:
 */

public interface SubjectCategoryDomainService {

     void add(SubjectCategoryBO subjectCategoryBo);




     List<SubjectCategoryBO> queryCategory(SubjectCategoryBO subjectCategoryBO);

     Boolean update(SubjectCategoryBO subjectCategoryBO);

     Boolean delete(SubjectCategoryBO subjectCategoryBO);

}
