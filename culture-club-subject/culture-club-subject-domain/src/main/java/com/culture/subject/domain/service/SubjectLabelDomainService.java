package com.culture.subject.domain.service;

import com.culture.subject.domain.entity.SubjectCategoryBO;
import com.culture.subject.domain.entity.SubjectLabelBO;

import java.util.List;

/**
 * @Title: SubjectCategoryService
 * @Author sn
 * @Package com.culture.subject.domain.service
 * @Date 2024/3/6 14:13
 * @description:
 */

public interface SubjectLabelDomainService {


     Boolean add(SubjectLabelBO subjectLabelBO);

     Boolean update(SubjectLabelBO subjectLabelBO);

     Boolean delete(SubjectLabelBO subjectLabelBO);

     List<SubjectLabelBO> queryLabelByCategoryId(SubjectLabelBO subjectLabelBO);
}
