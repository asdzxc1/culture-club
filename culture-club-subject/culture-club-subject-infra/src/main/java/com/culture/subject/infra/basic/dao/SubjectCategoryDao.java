package com.culture.subject.infra.basic.dao;

import com.culture.subject.infra.basic.entity.SubjectCategory;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * 题目分类(SubjectCategory)表数据库访问层
 *
 * @author makejava
 * @since 2024-03-05 17:02:15
 */
public interface SubjectCategoryDao {

    /**
     * 新增数据
     *
     * @param id 实例对象
     * @return 影响行数
     */
    SubjectCategory queryById(Long id);

    /**
     * 新增数据
     *
     * @param subjectCategory 实例对象
     * @return 影响行数
     */
    int insert(SubjectCategory subjectCategory);

    /**
     * 修改数据
     *
     * @param subjectCategory 实例对象
     * @return 影响行数
     */
    int update(SubjectCategory subjectCategory);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(Long id);


    List<SubjectCategory> queryCategory(SubjectCategory subjectCategory);

}