package com.culture.subject;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * @Title: SubjectApplication刷题微服务启动类
 * @Author sn
 * @Package com.culture.subject
 * @Date 2024/3/3 21:06
 * @description:
 */

@SpringBootApplication
@ComponentScan("com.culture")
@MapperScan("com.culture.**.dao")
public class SubjectApplication {
    public static void main(String[] args) {
        SpringApplication.run(SubjectApplication.class, args);
    }
}
